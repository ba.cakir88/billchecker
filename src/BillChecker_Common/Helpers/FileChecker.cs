﻿using BillChecker_Common.Base;
using Microsoft.AspNetCore.Http;

namespace BillChecker_Common.Helpers
{
    public class FileChecker
    {
        public static bool CheckTheFile( IFileChecker fileChecker, IFormFile file)
        {
            return fileChecker.CheckFileHex(file);
        }        
    }
}
