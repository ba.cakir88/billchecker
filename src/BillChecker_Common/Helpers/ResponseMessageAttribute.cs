﻿using System;
using System.Reflection;
using System.Linq;

namespace BillChecker_Common.Helpers
{
    public class ResponseMessageAttribute : Attribute
    {
        private string _value;
        public ResponseMessageAttribute(string value)
        {
            _value = value;
        }
        public string Value
        {
            get { return _value; }
        }

        public static string GetStringValue(Enum value)
        {
            Type type = value.GetType();
            FieldInfo fi = type.GetField(value.ToString());
            return (fi.GetCustomAttributes(typeof(ResponseMessageAttribute), false).FirstOrDefault() as ResponseMessageAttribute).Value;
        }
    }
}
