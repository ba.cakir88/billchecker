﻿using BillChecker_Common.Base;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;

namespace BillChecker_Common.FileCheckers
{
    public class ExcelFileChecker : IFileChecker
    {
        public bool CheckFileHex(IFormFile file)
        {            
            int headerSize = 4;
            byte[] bytesFile = new byte[4];
            using (var fs = file.OpenReadStream())
            {
                fs.Read(bytesFile, 0, headerSize);
                fs.Close();
            }
            string hex = BitConverter.ToString(bytesFile);
            string[] headerArray = hex.Split(new Char[] { '-' }).ToArray();
            string header = headerArray[0] + headerArray[1] + headerArray[2] + headerArray[3];
            if ((header == "D0CF11E0") || (header == "504B0304"))
            {
                return true;
            }
            return false;
        }
    }
}
