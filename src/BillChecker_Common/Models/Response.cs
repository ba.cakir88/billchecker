﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BillChecker_Common.Models
{
    public class Response
    {
        public bool Success { get; set; }
        public IEnumerable<object> ResponseData { get; set; }
        public string ResponseMessage { get; set; }
    }
}
