﻿using BillChecker_Common.Helpers;

namespace BillChecker_Common.Enums
{
    public enum ResponseMessage
    {
        [ResponseMessage("Process done succesfully")]
        Success,
        [ResponseMessage("There is no convenient proposal file processor")]
        NoConvenientProposalProcessor,
        [ResponseMessage("Something went wrong while reading proposal file!!!")]
        ProposalProcessorError,
        [ResponseMessage("There is no convenient bill file processor")]
        NoConvenientBillProcessor,
        [ResponseMessage("Something went wrong while processing bill file!!!")]
        BillProcessorError,
    }
}
