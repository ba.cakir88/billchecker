﻿using Microsoft.AspNetCore.Http;

namespace BillChecker_Common.Base
{
    public interface IFileChecker
    {
        bool CheckFileHex(IFormFile file);
    }
}
