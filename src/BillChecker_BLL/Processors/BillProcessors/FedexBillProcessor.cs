﻿using BillChecker_BLL.Enums;
using BillChecker_BLL.Interfaces;
using BillChecker_BLL.Models;
using IronXL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace BillChecker_BLL.BillProcessors
{
    public class FedexBillProcessor : IBillProcessor
    {
        private WorkSheet sheet;
        private List<Proposal> proposal;
        public IEnumerable<IInvoice> ProcessBillFile(string billFile, List<Proposal> proposalList)
        {
            List<FedexInvoice> invoiceList = new List<FedexInvoice>();
            proposal = proposalList;

            try
            {
                var pathName = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName + "\\BillChecker\\BillChecker_Common\\Files\\";
                WorkBook workbook = WorkBook.Load(pathName + billFile);
                sheet = workbook.WorkSheets[0];

                for (int i = 2; i <= sheet.RowCount; i++)
                {
                    FedexInvoice invoice = new FedexInvoice();
                    invoice.InvoiceNumber = sheet[$"C{i}"].Value.ToString();
                    invoice.GroundTrackingId = sheet[$"I{i}"].Value.ToString();
                    invoice.ShipmentDate = sheet[$"N{i}"].Value.ToString();
                    invoice.ShiperName = sheet[$"AP{i}"].Value.ToString();
                    invoice.RecipientName = sheet[$"AG{i}"].Value.ToString();
                    invoice.ServiceType = sheet[$"L{i}"].Value.ToString();
                    invoice.Zone = sheet[$"BL{i}"].Value.ToString();
                    invoice.RatedWeightAmount = decimal.Parse(sheet[$"U{i}"].Value.ToString(), new CultureInfo("en-US"));
                    invoice.NetChargeAmount = decimal.Parse(sheet[$"K{i}"].Value.ToString(), new CultureInfo("en-US"));

                    invoice.ExtraCost = GetExtraCost(i);
                    decimal baseProposalAmount = GetBaseProposalAmount(invoice.Zone, invoice.RatedWeightAmount, invoice.ServiceType);
                    decimal calculatedRate = baseProposalAmount + invoice.ExtraCost;

                    invoice.CalculatedRate = calculatedRate;

                    invoiceList.Add(invoice);
                }

            }
            catch (Exception ex)
            {
                // TODO: add log
                throw;
            }

            return invoiceList;
        }
        private decimal GetBaseProposalAmount(string zone, decimal ratedWeightAmount, string serviceType)
        {
            decimal calculatedRate = 0;
            Proposal objProposal = null;
            string _serviceType = serviceType.Contains("Priority") ? ServiceTypes.Priority.ToString() : ServiceTypes.Economy.ToString();
            int weight = (int)Math.Ceiling(ratedWeightAmount);

            if (ratedWeightAmount >= 100 && ratedWeightAmount < 154)
            {
                weight = 100;
            }

            if (ratedWeightAmount >= 155 && ratedWeightAmount < 1000)
            {
                weight = 155;
            }

            if (ratedWeightAmount >= 1000 && ratedWeightAmount < 99999)
            {
                weight = 1000;
            }

            objProposal = proposal.Where(p => p.Weight == weight
                                                && p.ServiceType == _serviceType
                                                && p.Zone == zone).FirstOrDefault();

            if (weight >= 100)
                calculatedRate = (objProposal != null ? objProposal.Cost : 0) * ratedWeightAmount;
            else
                calculatedRate = objProposal != null ? objProposal.Cost : 0;

            return calculatedRate;
        }
        private decimal GetExtraCost(int index)
        {
            decimal sum = 0;
            var cells = sheet[$"DC{index}:EY{index}"];
            decimal cost = 0;

            foreach (var item in cells)
            {
                
                decimal.TryParse(item.Value.ToString().Replace(',','.'), NumberStyles.AllowDecimalPoint | NumberStyles.Number, new CultureInfo("en-US"), out cost);
                if (cost > 0)
                    sum += cost;
            }

            return sum;
        }
    }
}
