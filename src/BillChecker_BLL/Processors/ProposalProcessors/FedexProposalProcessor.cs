﻿using BillChecker_BLL.Base;
using BillChecker_BLL.Enums;
using BillChecker_BLL.Models;
using IronXL;
using System;
using System.Collections.Generic;
using System.IO;

namespace BillChecker_BLL.ProposalProcessors
{
    public class FedexProposalProcessor : IProposalProcessor
    {
        List<Proposal> proposalValueList;

        public List<Proposal> GetProposal(string proposalFile)
        {
            proposalValueList = new List<Proposal>();
            string serviceType = "";
            int weight = 0;
            int zoneCount = 0;

            var pathName = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName + "\\BillChecker\\BillChecker_Common\\Files\\";
            WorkBook workbook = WorkBook.Load(pathName + proposalFile);
            WorkSheet sheet = workbook.GetWorkSheet("Express");

            #region read external priority
            serviceType = ServiceTypes.Priority.ToString();
            for (int i = 10; i < 109; i++)
            {
                zoneCount = 1;
                weight = sheet[$"A{i}"].IntValue;
                var cells = sheet[$"B{i}:P{i}"];

                foreach (var cell in cells)
                {
                    Proposal proposal = new Proposal
                    {
                        ServiceType = serviceType,
                        Weight = weight,
                        Zone = Enum.GetName(typeof(Zones), zoneCount),
                        Cost = cell.DecimalValue
                    };

                    proposalValueList.Add(proposal);
                    zoneCount++;
                }
            }
            #endregion

            #region read external economy

            serviceType = ServiceTypes.Economy.ToString();
            for (int i = 123; i < 222; i++)
            {
                zoneCount = 1;
                weight = sheet[$"A{i}"].IntValue;
                var cells = sheet[$"B{i}:P{i}"];

                foreach (var cell in cells)
                {
                    Proposal proposal = new Proposal
                    {
                        ServiceType = serviceType,
                        Weight = weight,
                        Zone = Enum.GetName(typeof(Zones), zoneCount),
                        Cost = cell.DecimalValue
                    };

                    proposalValueList.Add(proposal);
                    zoneCount++;
                }
            }
            #endregion

            #region read external priority multiply rate
            serviceType = ServiceTypes.Priority.ToString();
            for (int i = 113; i < 116; i++)
            {
                zoneCount = 1;
                weight = int.Parse(sheet[$"A{i}"].Value.ToString().Split('-')[0].Trim());
                var cells = sheet[$"B{i}:P{i}"];

                foreach (var cell in cells)
                {
                    Proposal proposal = new Proposal
                    {
                        ServiceType = serviceType,
                        Weight = weight,
                        Zone = Enum.GetName(typeof(Zones), zoneCount),
                        Cost = cell.DecimalValue
                    };

                    proposalValueList.Add(proposal);
                    zoneCount++;
                }
            }
            #endregion

            #region read external economy multiply rate

            serviceType = ServiceTypes.Economy.ToString();
            for (int i = 226; i < 229; i++)
            {
                zoneCount = 1;
                weight = int.Parse(sheet[$"A{i}"].Value.ToString().Split('-')[0].Trim());
                var cells = sheet[$"B{i}:P{i}"];

                foreach (var cell in cells)
                {
                    Proposal proposal = new Proposal
                    {
                        ServiceType = serviceType,
                        Weight = weight,
                        Zone = Enum.GetName(typeof(Zones), zoneCount),
                        Cost = cell.DecimalValue
                    };

                    proposalValueList.Add(proposal);
                    zoneCount++;
                }
            }
            #endregion

            return proposalValueList;
        }
    }
}
