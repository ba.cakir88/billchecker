﻿using BillChecker_BLL.Interfaces;
using System;

namespace BillChecker_BLL.Base
{
    public class Invoice : IInvoice
    {
        public string InvoiceNumber { get; set; }
        public string GroundTrackingId { get; set; }
        public string ShipmentDate { get; set; }
        public string ShiperName { get; set; }
        public string RecipientName { get; set; }
    }
}
