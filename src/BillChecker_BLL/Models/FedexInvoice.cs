﻿using BillChecker_BLL.Base;

namespace BillChecker_BLL.Models
{
    public class FedexInvoice: Invoice
    {
        public string ServiceType { get; set; }
        public string Zone { get; set; }
        public decimal RatedWeightAmount { get; set; }
        public decimal ExtraCost { get; set; }
        public decimal NetChargeAmount { get; set; }
        public decimal CalculatedRate { get; set; }
    }
}
