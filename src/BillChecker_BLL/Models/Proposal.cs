﻿namespace BillChecker_BLL.Models
{
    public class Proposal
    {
        public string ServiceType { get; set; }
        public int Weight { get; set; }
        public string Zone { get; set; }
        public decimal Cost { get; set; }
    }
}
