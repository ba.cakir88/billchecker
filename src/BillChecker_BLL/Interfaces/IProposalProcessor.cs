﻿using BillChecker_BLL.Models;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace BillChecker_BLL.Base
{
    public interface IProposalProcessor
    {
        List<Proposal> GetProposal(string proposalFile);
    }
}
