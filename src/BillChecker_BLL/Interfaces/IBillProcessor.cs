﻿using BillChecker_BLL.Base;
using BillChecker_BLL.Models;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace BillChecker_BLL.Interfaces
{
    public interface IBillProcessor
    {
        IEnumerable<IInvoice> ProcessBillFile(string billFile, List<Proposal> proposalList);
    }
}
