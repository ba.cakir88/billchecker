﻿using BillChecker_BLL.Base;
using BillChecker_BLL.BillProcessors;
using BillChecker_BLL.Interfaces;
using BillChecker_BLL.Models;
using BillChecker_BLL.ProposalProcessors;
using BillChecker_Common.Enums;
using BillChecker_Common.Helpers;
using BillChecker_Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BillChecker_BLL
{
    public class ProcessFiles
    {
        private readonly string _companyName;
        public ProcessFiles(string companyName)
        {
            _companyName = companyName;
        }
        public Response CheckBillFileByProposalFile(string billFileName, string proposalFileName)
        {
            Response response = new Response();

            #region get proposal processor
            IProposalProcessor proposalProcessor = GetProposalProcessor();
            if (proposalProcessor == null)
            {
                response.Success = false;
                response.ResponseMessage = ResponseMessageAttribute.GetStringValue(ResponseMessage.NoConvenientProposalProcessor);

                return response;
            }
            #endregion

            #region process proposal file
            var proposalData = GetProposalFile(proposalFileName, proposalProcessor);
            if (proposalData == null)
            {
                response.Success = false;
                response.ResponseMessage = ResponseMessageAttribute.GetStringValue(ResponseMessage.ProposalProcessorError);

                return response;
            }
            #endregion

            #region get bill processor
            IBillProcessor billProcessor = GetBillProcessor();
            if (billProcessor == null)
            {
                response.Success = false;
                response.ResponseMessage = ResponseMessageAttribute.GetStringValue(ResponseMessage.NoConvenientBillProcessor);

                return response;
            }
            #endregion

            #region process bill file
            var invoice = ProcessBillFile(billFileName, proposalData, billProcessor);
            if (invoice == null)
            {
                response.Success = false;
                response.ResponseMessage = ResponseMessageAttribute.GetStringValue(ResponseMessage.BillProcessorError);

                return response;
            }
            #endregion

            response.Success = true;
            response.ResponseMessage = ResponseMessageAttribute.GetStringValue(ResponseMessage.Success);
            response.ResponseData = invoice;

            return response;
        }
        private IBillProcessor GetBillProcessor()
        {
            IBillProcessor billProcessor = null;

            switch (_companyName)
            {
                case "Fedex":
                    billProcessor = new FedexBillProcessor();
                    break;
                default:
                    break;
            }
            
            return billProcessor;
        }
        private List<IInvoice> ProcessBillFile(string billFileName, List<Proposal> dicProposal, IBillProcessor billProcessor)
        {
            List<IInvoice> processedInvoiceData = null;

            try
            {
                processedInvoiceData = billProcessor.ProcessBillFile(billFileName, dicProposal).ToList();
            }
            catch (Exception ex)
            {

            }
            return processedInvoiceData;
        }
        private List<Proposal> GetProposalFile(string proposalFileName, IProposalProcessor proposalProcessor)
        {
            List<Proposal> proposalData = null;
            try
            {
                proposalData = proposalProcessor.GetProposal(proposalFileName);
            }
            catch (Exception ex)
            {

            }
            return proposalData;
        }
        private IProposalProcessor GetProposalProcessor()
        {
            IProposalProcessor proposalProcessor = null;

            switch (_companyName)
            {
                case "Fedex":
                    proposalProcessor = new FedexProposalProcessor();
                    break;
                default:
                    break;
            }

            return proposalProcessor;
        }
    }
}
