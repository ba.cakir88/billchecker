﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

$(document).ready(function () {
    bsCustomFileInput.init();
    $('#invoicedatable').DataTable({
        dom: 'Bfrtip',
        responsive: true,
        fixedHeader: true,
        buttons: [
            'excelHtml5'
        ],
        rowCallback: function (row, data, index) {
            
            if (data[9] === data[10]) {
                $('td', row).css('background-color', '#28a745');
            }

            if (data[9] > data[10]) {
                $('td', row).css('background-color', '#f3b76e');
            }

            if (data[9] < data[10]) {
                $('td', row).css('background-color', '#dcb5b9');
            }
        }
    });
});

$(function () {

    $('#btnupload').on('click', function () {
        var fileExtension = ['xls', 'xlsx'];
        var billname = $('#billupload').val();

        if (billname.length == 0) {
            alert("Please select a bill file.");
            return false;
        }
        else {
            var extension = billname.replace(/^.*\./, '');
            if ($.inArray(extension, fileExtension) == -1) {
                alert("Please select only excel files.");
                return false;
            }
        }
        var fdata = new FormData();
        var fileUpload1 = $("#billupload").get(0);

        if (fileUpload1.files[0].size) {
            fdata.append(fileUpload1.files[0].name, fileUpload1.files[0]);
        }
        else {
            alert("Uploaded files can not be empty");
            return false;
        }

        $.ajax({
            url: '/Home/UploadBillFile',
            type: "POST",
            contentType: false, // Not to set any content header    
            processData: false, // Not to process data    
            data: fdata,
            async: false,
            success: function (result) {
                if (result != "") {                    
                    $('#btnupload').prop("disabled", true);
                    $('#billupload').prop("disabled", true);
                    LoadProgressBar();
                }
            },
            error: function (err) {
                alert(err.statusText);
            }
        });

    });

    $('#btnuploadproposal').on('click', function () {
        var fileExtension = ['xls', 'xlsx'];
        var proposalname = $('#proposalupload').val();

        if (proposalname.length == 0) {
            alert("Please select a proposal file.");
            return false;
        }
        else {
            var extension = proposalname.replace(/^.*\./, '');
            if ($.inArray(extension, fileExtension) == -1) {
                alert("Please select only excel files.");
                return false;
            }
        }

        var fdata = new FormData();
        var fileUpload = $("#proposalupload").get(0);

        if (fileUpload.files[0].size > 0) {
            fdata.append(fileUpload.files[0].name, fileUpload.files[0]);
        }
        else {
            alert("Uploaded files can not be empty");
            return false;
        }

        $.ajax({
            url: '/Home/UploadProposalFile',
            type: "POST",
            contentType: false, // Not to set any content header    
            processData: false, // Not to process data    
            data: fdata,
            async: false,
            success: function (result) {
                if (result != "") {
                    $('#btnuploadproposal').prop("disabled", true);
                    $('#proposalupload').prop("disabled", true);
                    LoadProgressBar2();
                }
            },
            error: function (err) {
                alert(err.statusText);
            }
        });

    });

    $('#btnprocess').on('click', function () {
        var proposalname = $('#proposalupload').val();
        var billname = $('#billupload').val();

        if (billname.length == 0) {
            alert("Please select a bill file.");
            return false;
        }

        if (proposalname.length == 0) {
            alert("Please select a proposal file.");
            return false;
        }

        var datable = $('#invoicedatable').DataTable();
        $.ajax({
            url: '/Home/ProcessFiles',
            type: "POST",
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            data: { billFileName: $('#billupload').get(0).files[0].name, proposalFileName: $('#proposalupload').get(0).files[0].name, companyName: 'Fedex' },
            success: function (result) {
                if (result != "") {
                    for (var i = 0; i < result.responseData.length; i++) {
                        datable.row.add([
                            result.responseData[i].invoiceNumber,
                            result.responseData[i].groundTrackingId,
                            result.responseData[i].shipmentDate,
                            result.responseData[i].shiperName,
                            result.responseData[i].recipientName,
                            result.responseData[i].serviceType,
                            result.responseData[i].zone,
                            result.responseData[i].ratedWeightAmount,
                            result.responseData[i].extraCost,
                            result.responseData[i].netChargeAmount,
                            result.responseData[i].calculatedRate
                        ]).draw();
                    }

                    datable.columns([6]).every(function () {
                        var column = this;
                        var select = $('<select><option value=""></option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                    });
                };

                $('#btnuploadproposal').prop("disabled", false);
                $('#proposalupload').prop("disabled", false);
                $('#btnupload').prop("disabled", false);
                $('#billupload').prop("disabled", false);
            },
            error: function (err) {
                alert(err.statusText);
            }
        });



        //$('#invoicedatable').DataTable({
        //    ajax: {
        //        type: "POST",
        //        url: '/Home/ProcessFiles',
        //        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        //        data: { billFileName: $('#billupload').get(0).files[0].name, proposalFileName: $('#proposalupload').get(0).files[0].name, companyName: 'Fedex' },
        //        dataSrc: 'responseData',
        //        success: function (result) {
        //            if (result != "") {
        //                alert(result.responseData[0].calculatedRate);
        //            }
        //        },
        //        error: function (x, y) {
        //            console.log(x);
        //        }
        //    },
        //    columns: [{
        //        data: 'calculatedRate'
        //    }, {
        //        data: 'calculatedRate'
        //    }, {
        //        data: 'calculatedRate'
        //    }, {
        //        data: 'calculatedRate'
        //    }, {
        //        data: 'calculatedRate'
        //    }, {
        //        data: 'calculatedRate'
        //    }, {
        //        data: 'calculatedRate'
        //    }, {
        //        data: 'calculatedRate'
        //    }, {
        //        data: 'calculatedRate'
        //    }, {
        //        data: 'calculatedRate'
        //    }, {
        //        data: 'calculatedRate'
        //    }]
        //    rowCallback: function (row, data, index) {
        //        if (data.netChargeAmount == data.calculatedRate) {
        //            $(row).find('td:eq(5)').css('color', '#28a745');
        //        }

        //        if (data.netChargeAmount > data.calculatedRate) {
        //            $(row).find('td:eq(5)').css('color', '#dcb5b9');
        //        }

        //        if (data.netChargeAmount < data.calculatedRate) {
        //            $(row).find('td:eq(5)').css('color', '#f3b76e');
        //        }
        //    }
        //});

    });

    function LoadProgressBar() {
        var progressbar = $("#progressbar-5");
        var progressLabel = $(".progress-label");

        progressbar.show();
        $("#progressbar-5").progressbar({
            //value: false,  
            change: function () {
                progressLabel.text(
                    progressbar.progressbar("value") + "%");  // Showing the progress increment value in progress bar  
            },
            complete: function () {
                progressLabel.text("Loading Completed!");
                progressbar.progressbar("value", 0);  //Reinitialize the progress bar value 0  
                progressLabel.text("");
                progressbar.hide(); //Hiding the progress bar
                $("#btnuploadproposal")
            }
        });
        function progress() {
            var val = progressbar.progressbar("value") || 0;
            progressbar.progressbar("value", val + 1);
            if (val < 99) {
                setTimeout(progress, 25);
            }
        }
        setTimeout(progress, 100);
    }

    function LoadProgressBar2() {
        var progressbar = $("#progressbar-6");
        var progressLabel = $(".progress-label2");
        progressbar.show();
        $("#progressbar-6").progressbar({
            //value: false,  
            change: function () {
                progressLabel.text(
                    progressbar.progressbar("value") + "%");  // Showing the progress increment value in progress bar  
            },
            complete: function () {
                progressLabel.text("Loading Completed!");
                progressbar.progressbar("value", 0);  //Reinitialize the progress bar value 0  
                progressLabel.text("");
                progressbar.hide(); //Hiding the progress bar
            }
        });
        function progress() {
            var val = progressbar.progressbar("value") || 0;
            progressbar.progressbar("value", val + 1);
            if (val < 99) {
                setTimeout(progress, 25);
            }
        }
        setTimeout(progress, 100);
    }
})