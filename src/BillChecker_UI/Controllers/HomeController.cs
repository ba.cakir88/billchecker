﻿using BillChecker_BLL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Linq;

namespace BillChecker_UI.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult ProcessFiles(string billFileName, string proposalFileName, string companyName)
        {
            ProcessFiles processFiles = new ProcessFiles(companyName);
            var response = processFiles.CheckBillFileByProposalFile(billFileName, proposalFileName);

            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> UploadBillFile()
        {
            string fname;
            try
            {
                IFormFile file = Request.Form.Files[0];
                fname = file.FileName;
                var pathName = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName + "\\BillChecker\\BillChecker_Common\\Files";
                var filePath = Path.Combine(pathName, fname);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }

                _logger.LogInformation(string.Format("{0} file has been downloanded", fname));
                return Json("File Uploaded Successfully!");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Something went wrong while file uploading");
                return Json("Error occurred. Error details: " + ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> UploadProposalFile()
        {
            string fname;
            try
            {
                IFormFile file = Request.Form.Files[0];
                fname = file.FileName;
                var pathName = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName + "\\BillChecker\\BillChecker_Common\\Files";
                var filePath = Path.Combine(pathName, fname);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }

                _logger.LogInformation(string.Format("{0} file has been downloanded", fname));
                return Json("File Uploaded Successfully!");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Something went wrong while file uploading");
                return Json("Error occurred. Error details: " + ex.Message);
            }
        }
    }
}
